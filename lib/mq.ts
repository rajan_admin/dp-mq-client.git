import { IClientPublishOptions, MqttClient, Packet } from "mqtt";
import { Injectable, Single } from "dp-ioc"

export interface MqOption {
    topic: string,
    option?: IClientPublishOptions
}

export function MqMessage(option: MqOption) {
    return function (target: any) {
        target.$_mq_msg_option = option;
    }
}

export interface PublishCallBack {
    (error: Error | undefined, packet: Packet | undefined): void
}

export interface SubscribeCallBack<T> {
    (mssage: T): void
}


export function metaUpdate(obj:any,MqOption:MqOption){
    obj.constructor.$_mq_msg_option = MqOption;
}

@Single()
@Injectable()
export class DpMqClient {
    private mqttClient: MqttClient | null;

    private onMessageCb: Map<string, Array<{ key: Symbol, fn: Function }>> = new Map();

    init(client: MqttClient) {
        this.mqttClient = client;
        this.mqttClient.on("message", (topic: string, message: string) => {
            let fnArr = this.onMessageCb.get(topic);
            // console.log("mq on msg",fnArr);
            if (fnArr) {
                fnArr.forEach(fn => {
                    fn.fn(JSON.parse(message.toString()));
                })
            }
        })
    }

    publish(msg: any, fn?: PublishCallBack): { error: null | Error } {
        if (!this.mqttClient) {
            return {
                error: new Error("mqttClient is null,publish failed")
            }
        }
        if (msg && msg.constructor && msg.constructor.$_mq_msg_option) {
            let topic = msg.constructor.$_mq_msg_option.topic;
            if (!topic) {
                return {
                    error: new Error("pubish error, because topic of message is invalide")
                }
            }
            let option = msg.constructor.$_mq_msg_option.option ? msg.constructor.$_mq_msg_option.option : {};
            let cb = fn ? fn : (error: Error | undefined, packet: Packet | undefined) => { };
            this.mqttClient.publish(topic, JSON.stringify(msg), option, cb);
        } else {
            return {
                error: new Error("pubish error, because message is invalide object")
            }
        }
        return { error: null };
    }

    subscribe<T>(msg: any, cb: SubscribeCallBack<T>): { error: null | Error } {
        if (!this.mqttClient) {
            return {
                error: new Error("mqttClient is null,subscribe failed.")
            }
        }
        if (msg && msg.constructor && msg.constructor.$_mq_msg_option) {
            let topic = msg.constructor.$_mq_msg_option.topic;
            if (!topic) {
                return {
                    error: new Error("subscribe error, because topic of message is invalide")
                }
            }
            msg.$_symbol_key = Symbol();
            this.registerMessageCb(msg, cb);
            this.mqttClient.subscribe(topic);
        } else {
            return {
                error: new Error("subscribe error, because message is invalide object")
            }
        }
        return {
            error: null,
        }
    }

    unsubscribe(msg: any,): { error: null | Error } {
        if (!this.mqttClient) {
            return {
                error: new Error("mqttClient is null,unsubscribe failed.")
            }
        }
        if (!msg.$_symbol_key) {
            return {
                error: new Error("this message object is not be subscribed")
            }
        }
        if (msg && msg.constructor && msg.constructor.$_mq_msg_option) {
            let topic = msg.constructor.$_mq_msg_option.topic;
            if (!topic) {
                return {
                    error: new Error("unsubscribe error, because topic of message is invalide")
                }
            }
            let arr = this.onMessageCb.get(topic);
            if (arr) {
                let newArr = [];
                for (let fn of arr) {
                    if (fn) {
                        if (fn.key != msg.$_symbol_key) {
                            newArr.push(fn)
                        }
                    }
                }
                //表示 没有任何消息体订阅该主题，则 可以取消订阅
                if (newArr.length == 0) {
                    this.mqttClient.unsubscribe(topic)
                }
                this.onMessageCb.set(topic, newArr);
            }
        } else {
            return {
                error: new Error("subscribe error, because message is invalide object")
            }
        }
        return {
            error: null,
        }
    }

    private registerMessageCb(msg: any, fn: Function) {
        let topic = msg.constructor.$_mq_msg_option.topic;
        let key = msg.$_symbol_key;
        let cbArr = this.onMessageCb.get(topic);
        if (!cbArr) {
            cbArr = []
        }
        cbArr.push({
            key,
            fn,
        });
        this.onMessageCb.set(topic, cbArr);
    }


}