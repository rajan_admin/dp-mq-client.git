const assert = require("assert");
import { MqttClient, connect } from "mqtt";
import { DpMqClient, metaUpdate, MqMessage, MqOption } from "../lib/mq"

describe('DPMQ Client test', function () {
    describe('#Publish', function () {

        it('基本发布订阅测试', function (done) {
            return done();
            @MqMessage({
                topic: "a/b/c",
            })
            class TestMsg {
                constructor(
                    public name: string,
                    public age: number
                ) { }
            }
            let mqttClient = connect("mqtt://127.0.0.1", { port: 10084, password: "Hlvwax@IS2HcxG^y", username: "message_queue_20211105" });
            mqttClient.on("connect", () => {
                let dpMq = new DpMqClient();
                dpMq.init(mqttClient);
                let testMsg = new TestMsg("rajan", 18);
                dpMq.subscribe<TestMsg>(testMsg, (msg) => {
                    assert.equal(msg.name, testMsg.name, "发布的消息和监听到的消息不一致");
                    assert.equal(msg.age, testMsg.age, "发布的消息和监听到的消息不一致");
                    done();
                    setTimeout(() => {
                        process.exit();
                    }, 5000)
                })
                dpMq.publish(testMsg);
            })
        })


        it('单一消息类多对象订阅测试', function (done) {
            return done();
            @MqMessage({
                topic: "/a/b/c",
            })
            class TestMsg {
                constructor(
                    public name: string,
                    public age: number
                ) { }
            }
            let mqttClient = connect("mqtt://127.0.0.1", { port: 10084, password: "Hlvwax@IS2HcxG^y", username: "message_queue_20211105" });
            mqttClient.on("connect", () => {

                let testMsg1 = new TestMsg("rajan1", 19);
                let testMsg2 = new TestMsg("rajan2", 20);


                let dpMq = new DpMqClient();
                dpMq.init(mqttClient);



                let count = 0;
                dpMq.subscribe<TestMsg>(testMsg1, (msg) => {
                    if (msg.age == 19 || msg.age == 20) {
                        count++;
                    }
                    if (count == 4) {
                        done()
                    }
                })
                dpMq.subscribe<TestMsg>(testMsg2, (msg) => {
                    if (msg.age == 19 || msg.age == 20) {
                        count++;
                    }
                    if (count == 4) {
                        done()
                    }
                })

                dpMq.publish(testMsg1);
                dpMq.publish(testMsg2);

            })
        })
        it('单一消息类多对象订阅测试2', function (done) {
            return done();
            @MqMessage({
                topic: "/a/b/c",
            })
            class TestMsg {
                constructor(
                    public name: string,
                    public age: number
                ) { }
            }
            let mqttClient = connect("mqtt://127.0.0.1", { port: 10084, password: "Hlvwax@IS2HcxG^y", username: "message_queue_20211105" });
            mqttClient.on("connect", () => {

                let testMsg1 = new TestMsg("rajan1", 19);
                let testMsg2 = new TestMsg("rajan2", 20);

                let dpMq = new DpMqClient();
                dpMq.init(mqttClient);

                let count = 0;
                new Promise((rs) => {
                    dpMq.subscribe<TestMsg>(testMsg1, (msg) => {
                        if (msg.age == 19 || msg.age == 20) {
                            count++;
                        }
                        if (count == 4) {
                            rs(count)
                        }
                    })
                    dpMq.subscribe<TestMsg>(testMsg2, (msg) => {

                        if (msg.age == 19 || msg.age == 20) {
                            count++;
                        }
                        if (count == 4) {
                            rs(count)
                        }
                    })
                    dpMq.publish(testMsg1);
                    dpMq.publish(testMsg2);
                }).then(() => {
                    // 取消一次
                    count = 0;
                    dpMq.unsubscribe(testMsg1);
                    dpMq.publish(testMsg1);
                    dpMq.publish(testMsg2);
                    setTimeout(() => {
                        assert.equal(count, 2, "期望触发的次数不一致");
                        done();
                    }, 500);
                })


            })
        })

        it('取消尚未订阅的消息体', function (done) {
            return done()
            @MqMessage({
                topic: "/a/b/c",
            })
            class TestMsg {
                constructor(
                    public name: string,
                    public age: number
                ) { }
            }
            let mqttClient = connect("mqtt://127.0.0.1", { port: 10084, password: "Hlvwax@IS2HcxG^y", username: "message_queue_20211105" });
            mqttClient.on("connect", () => {

                let testMsg1 = new TestMsg("rajan1", 19);
                let dpMq = new DpMqClient();
                dpMq.init(mqttClient);
                let unsResult = dpMq.unsubscribe(testMsg1);
                assert.deepEqual(unsResult.error, new Error("this message object is not be subscribed"), "期望的是异常");
                done()
            })
        })

        it('动态更新meta信息', function (done) {
            // return done()
            @MqMessage({
                topic: "/a/b/c",
            })
            class TestMsg {
                public name: string
                public age: number
            }
            let testMsg1 = new TestMsg();
            // let dpMq = new DpMqClient();
            let option:MqOption = {
                topic: "/a/b/c",
                option:{
                    qos:1,
                    retain:true,
                }
            }
            metaUpdate(testMsg1,option);
            let constructor = testMsg1.constructor as any;            
            assert.deepEqual(constructor.$_mq_msg_option, option, "修改后meta信息不一致");
            done()
        })

    });
});